use std::fs::File;
use std::io::{self, BufRead};

fn main() {

    let file = File::open("./elfs.txt").unwrap();
    let lines : Vec<_> = io::BufReader::new(file).lines().collect();

    let mut elfs = lines
        .split(|character| character.as_ref().unwrap() == "")
        .map(|elf| {
            elf.iter().fold(0, |sum, cal| {
                sum + cal.as_ref().unwrap().parse::<i32>().unwrap()
            })
        }).fold([0,0,0], |max, v| { let &mut min = max.iter().min().unwrap(); min = v });
//        .collect::<Vec<i32>>(); //.sort();

//    elfs.sort();
//
//    let top_elfs = elfs
//        .windows(3)
//        .last()
//        .unwrap()
//        .iter()
//        .fold(0, |sum, v| sum + v);

    println! {"{}", elfs.iter().sum()};
}
