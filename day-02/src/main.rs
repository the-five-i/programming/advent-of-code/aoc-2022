use std::fs::File;
use std::io::{self, BufRead};

#[derive(PartialEq, Copy, Clone, Debug)]
enum Move {
    Rock,
    Paper,
    Scisors,
}

impl Move {
    fn parse(buff: char) -> Option<Move> {
        use Move::*;
        match buff {
            'A' | 'X' => Some(Rock),
            'B' | 'Y' => Some(Paper),
            'C' | 'Z' => Some(Scisors),
            _ => None,
        }
    }

    fn loses_against(self) -> Move {
        use Move::*;
        match self {
            Rock => Paper,
            Paper => Scisors,
            Scisors => Rock,
        }
    }

    fn wins_against(self) -> Move {
        use Move::*;
        match self {
            Rock => Scisors,
            Paper => Rock,
            Scisors => Paper,
        }
    }
}

#[derive(PartialEq)]
enum Outcome {
    Win,
    Lose,
    Draw,
}

impl Outcome {
    fn parse(buff: char) -> Option<Outcome> {
        use Outcome::*;
        match buff {
            'X' => Some(Lose),
            'Y' => Some(Draw),
            'Z' => Some(Win),
            _ => None,
        }
    }
}

fn my_play(elf: Move, outcome: Outcome) -> Move {
    use Outcome::*;

    match outcome {
        Lose => elf.wins_against(),
        Draw => elf,
        Win => elf.loses_against(),
    }
}

fn score(elf: Move, me: Move) -> i32 {
    use Move::*;
    (match me {
        Rock => 1,
        Paper => 2,
        Scisors => 3,
    }) + (match (elf, me) {
        (Rock, Paper) => 6,
        (Rock, Scisors) => 0,
        (Paper, Scisors) => 6,
        (Paper, Rock) => 0,
        (Scisors, Rock) => 6,
        (Scisors, Paper) => 0,
        (x, y) if x == y => 3,
        _ => panic! {"Impossible to reach"},
    })
}

fn part_one() {
    let file = File::open("./moves.txt").unwrap();
    let score = io::BufReader::new(file).lines().fold(0, |sum, round| {
        let elf = Move::parse(round.as_ref().unwrap().chars().nth(0).unwrap()).unwrap();
        let me = Move::parse(round.as_ref().unwrap().chars().nth(2).unwrap()).unwrap();
        score(elf, me) + sum
    });

    println! {"{score}"}
}

fn part_two() {
    let file = File::open("./moves.txt").unwrap();
    let score = io::BufReader::new(file).lines().fold(0, |sum, round| {
        let elf = Move::parse(round.as_ref().unwrap().chars().nth(0).unwrap()).unwrap();
        let me = my_play(
            elf,
            Outcome::parse(round.as_ref().unwrap().chars().nth(2).unwrap()).unwrap(),
        );
        score(elf, me) + sum
    });

    println! {"{score}"}
}

fn main() {
    part_one();
    part_two();
}
