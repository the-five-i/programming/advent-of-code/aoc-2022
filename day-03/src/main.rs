use std::collections::HashSet;
use std::fs::File;
use std::io::{self, BufRead};

fn item_score(item: char) -> i32 {
    let score: i32;

    let value = item as i32;
    let a_lower = 'a' as i32;
    let z_lower = 'z' as i32;
    let a_upper = 'A' as i32;
    let z_upper = 'Z' as i32;

    if value >= a_lower && value <= z_lower {
        score = 1 + value - a_lower;
    } else if value >= a_upper && value <= z_upper {
        score = (1 + value - a_upper) + (1 + z_lower - a_lower);
    } else {
        panic! {"Unknown character!"};
    }

    score
}

fn misplaced_item(rucksack: &str) -> Option<char> {
    let mut set = HashSet::new();
    let size = rucksack.len();

    for (index, item) in rucksack.chars().enumerate() {
        if index < size / 2 {
            set.insert(item);
        } else {
            if let Some(misplaced) = set.get(&item) {
                return Some(*misplaced);
            }
        }
    }

    return None;
}

fn priority(rucksack: &str) -> i32 {
    let item_priotrity: i32;

    if let Some(item) = misplaced_item(rucksack) {
        item_priotrity = item_score(item);
    } else {
        panic! {"One line has not misplaced item!"};
    }

    item_priotrity
}

fn group_priorities(rucksacks: Vec<String>) -> i32 {
    let mut badge_sets = rucksacks.iter().map(|rucksack| {
        let mut set = HashSet::<char>::new();
        for item in rucksack.chars() {
            set.insert(item);
        }
        set
    });

    let common1 = badge_sets.next().unwrap();
    let mut common2 = HashSet::<char>::new();
    let mut common3 = HashSet::<char>::new();
    for c in badge_sets.next().unwrap().intersection(&common1) {
        common2.insert(*c);
    }
    for c in badge_sets.next().unwrap().intersection(&common2) {
        common3.insert(*c);
    }

    item_score(*common3.iter().next().unwrap())
}

fn part_one() {
    let file = File::open("./rucksacks.txt").unwrap();
    let priority = io::BufReader::new(file)
        .lines()
        .fold(0, |sum, rucksack| sum + priority(&rucksack.unwrap()));

    println! {"{priority}"}
}

fn part_two() {
    let file = File::open("./rucksacks.txt").unwrap();
    let mut lines = io::BufReader::new(file).lines();

    let mut sum = 0i32;

    'outer: loop {
        let mut group: Vec<_> = Vec::new();
        for _ in 0..3 {
            if let Some(rucksack) = lines.next() {
                group.push(rucksack.unwrap());
            } else {
                break 'outer;
            }
        }

        sum += group_priorities(group);
    }

    println! {"{sum}"};
}

fn main() {
    part_one();
    part_two();
}
