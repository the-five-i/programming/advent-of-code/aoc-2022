use std::fs::File;
use std::io::{self, BufRead};

fn is_contained(elf1: (i32,i32), elf2: (i32,i32)) -> bool {
    use std::cmp::Ordering::*;
    match ((elf1.0).cmp(&(elf2.0)), (elf1.1).cmp(&(elf2.1))) {
        (Less|Equal, Greater|Equal) => true,
        (Greater|Equal, Less|Equal) => true,
        _ => false,
    }
}

fn is_overlaped(elf1: (i32,i32), elf2: (i32,i32)) -> bool {
    use std::cmp::Ordering::*;
    match (
        (elf1.0).cmp(&(elf2.0)),
        (elf1.1).cmp(&(elf2.0)),
        (elf1.0).cmp(&(elf2.1)),
        (elf1.1).cmp(&(elf2.1))
        ) {
        (Less|Equal, Greater|Equal, _, _) => true,
        (Greater|Equal, Less|Equal, _, _) => true,
        (_, _, Less|Equal, Greater|Equal) => true,
        (_, _, Greater|Equal, Less|Equal) => true,
        (Less, _, _, Greater) => true,
        (Greater, _, _, Less) => true,
        _ => false,
    }
}

fn main() {
    let file = File::open("./pairs.txt").unwrap();
    let lines = io::BufReader::new(file).lines();

    let mut part_one = 0;
    let mut part_two = 0;

    for line in lines {
        let line = line.unwrap();
        let mut elfs = line.split(|sep| sep == ',');

        let mut elf1 = elfs.next().unwrap().split(|sep| sep == '-');
        let elf1 : (i32,i32) = 
            (elf1.next().unwrap().parse::<i32>().unwrap(),
            elf1.next().unwrap().parse::<i32>().unwrap());

        let mut elf2 = elfs.next().unwrap().split(|sep| sep == '-');
        let elf2 : (i32,i32) = 
            (elf2.next().unwrap().parse::<i32>().unwrap(),
            elf2.next().unwrap().parse::<i32>().unwrap());

        // Part one
        if is_contained(elf1, elf2) {
            part_one += 1;
        }

        // Part one
        if is_overlaped(elf1, elf2) {
            part_two += 1;
        }
    }

    println! {"{part_one}"};
    println! {"{part_two}"};
}
