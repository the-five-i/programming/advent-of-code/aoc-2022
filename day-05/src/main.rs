use std::fs::File;
use std::io::{self, BufRead};
use std::collections::LinkedList;

enum ReadSection {
    Stack,
    Moves,
}

fn move_part_one(crates: &mut Vec<LinkedList<char>>, quantity: usize, from: usize, to: usize) {
    for _ in 0..quantity {
        let value = (*crates)[from-1].pop_front().unwrap();
        (*crates)[to-1].push_front(value);
    }
}

fn move_part_two(crates: &mut Vec<LinkedList<char>>, quantity: usize, from: usize, to: usize) {
    let mut aux = LinkedList::new();
    for _ in 0..quantity {
        let value = (*crates)[from-1].pop_front().unwrap();
        aux.push_front(value);
    }
    for _ in 0..quantity {
        let value = aux.pop_front().unwrap();
        (*crates)[to-1].push_front(value);
    }
}

fn main() {
    let file = File::open("./crates.txt").unwrap();
    let lines = io::BufReader::new(file).lines();

    let mut state = ReadSection::Stack;
    let mut stack = LinkedList::<String>::new();
    let mut part_one : Vec<LinkedList<char>> = Vec::new();
    let mut part_two : Vec<LinkedList<char>> = Vec::new();

    for line in lines {
        let line = line.unwrap();

        if line == "" {
            if let Some(names) = stack.pop_back() {
                let lenght = names.split(|sep| sep == ' ').filter(|number| number.parse::<i32>().is_ok()).count();

                for _ in 0..lenght {
                    part_one.push(LinkedList::new());
                    part_two.push(LinkedList::new());
                }

                loop {
                    let layer = stack.pop_back();

                    if layer.is_none() {
                        break;
                    }

                    let layer : Vec<char> = layer.unwrap().chars().collect();

                    for i in 0..lenght {
                        let index = 1 + i * 4;
                        if layer[index] != ' ' {
                            part_one[i].push_front(layer[index]);
                            part_two[i].push_front(layer[index]);
                        }
                    }
                };

            } else {
                panic!{"Someting"};
            }

            state = ReadSection::Moves;
            continue;
        }

        match state {
            ReadSection::Stack => {
                stack.push_back(line);
            },
            ReadSection::Moves => {
                let parser = line.split(|sep| sep == ' ');
                let mut parser = parser.skip(1);
                let quantity : usize = parser.next().unwrap().parse().unwrap();
                let mut parser = parser.skip(1);
                let from : usize = parser.next().unwrap().parse().unwrap();
                let mut parser = parser.skip(1);
                let to : usize = parser.next().unwrap().parse().unwrap();

                move_part_one(&mut part_one, quantity, from, to);
                move_part_two(&mut part_two, quantity, from, to);
            },
        }
    }

    for mut s in part_one {
        print!{"{}", s.pop_front().unwrap()};
    }
    println!{""};

    for mut s in part_two {
        print!{"{}", s.pop_front().unwrap()};
    }
    println!{""};
}
