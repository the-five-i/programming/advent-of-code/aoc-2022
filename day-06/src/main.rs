use std::collections::HashSet;
use std::fs::File;
use std::io::{self, BufRead};

fn main() {
    let file = File::open("./message.txt").unwrap();
    let mut lines = io::BufReader::new(file).lines();

    if let Some(Ok(line)) = lines.next() {
        // Part One
        'window_part_one: for (id, section) in
            line.chars().collect::<Vec<_>>().windows(4).enumerate()
        {
            let mut set = HashSet::new();
            for c in section.iter() {
                if set.contains(c) {
                    continue 'window_part_one;
                }
                set.insert(c);
            }
            println! {"{}, {:?}", id+4, section};
            break 'window_part_one;
        }

        // Part Two
        'window_part_two: for (id, section) in
            line.chars().collect::<Vec<_>>().windows(14).enumerate()
        {
            let mut set = HashSet::new();
            for c in section.iter() {
                if set.contains(c) {
                    continue 'window_part_two;
                }
                set.insert(c);
            }
            println! {"{}, {:?}", id+14, section};
            break 'window_part_two;
        }
    }
}
