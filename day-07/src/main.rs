use std::collections::{HashMap, HashSet};
use std::fs::File;
use std::io::{self, BufRead};
use std::path::PathBuf;

fn main() {
    let file = File::open("./file_sistem.txt").unwrap();
    let lines = io::BufReader::new(file).lines();

    let mut working_dir = PathBuf::from("/");
    let mut dir_sizes: HashMap<PathBuf, usize> = HashMap::new();
    let mut parsed_files: HashSet<PathBuf> = HashSet::new();

    for line in lines {
        if let Ok(line) = line {
            let mut args = line.split(|sep| sep == ' ');
            match args.next().unwrap() {
                "$" => match args.next().unwrap() {
                    "cd" => match args.next().unwrap() {
                        ".." => {
                            working_dir.pop();
                        }
                        dir => working_dir.push(dir),
                    },
                    _ => continue,
                },
                "dir" => continue,
                size => {
                    let size = size.parse().unwrap();
                    let name = args.next().unwrap();
                    let mut current_file = working_dir.clone();
                    current_file.push(name);

                    if parsed_files.insert(current_file.clone()) {
                        loop {
                            if current_file.pop() {
                                if let Some(dir_size) = dir_sizes.get_mut(&current_file) {
                                    *dir_size += size;
                                } else {
                                    dir_sizes.insert(current_file.clone(), size);
                                }
                            } else {
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    let size : usize = dir_sizes
        .iter()
        .map(|(_, dir_size)| dir_size)
        .filter(|dir_size| **dir_size <= 100000)
        .sum();

    println! {"{size}"};

    let free_space;
    if let Some(root_size) = dir_sizes.get(&PathBuf::from("/")) {
        free_space = 70000000 - root_size;
    } else {
        panic! {"Failed reading root size"};
    }

    let remove_space = 30000000 - free_space;

    let size = dir_sizes
        .iter()
        .map(|(_, dir_size)| dir_size)
        .filter(|dir_size| **dir_size >= remove_space)
        .min_by(|s1, s2| s1.cmp(s2)).unwrap();
    println! {"{size}"};
}
