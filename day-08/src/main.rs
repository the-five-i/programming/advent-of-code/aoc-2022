use std::fs::File;
use std::io::{self, BufRead};

fn main() {
    let file = File::open("./forest.txt").unwrap();
    let lines = io::BufReader::new(file).lines();

    let mut forest_heights: Vec<Vec<usize>> = Vec::new();
    let mut forest_visibility: Vec<Vec<bool>> = Vec::new();

    let mut width = 0;
    let mut height = 0;

    // Read imput
    for (index, line) in lines.enumerate() {
        if let Ok(line) = line {
            width = line.chars().count();
            forest_heights.push(Vec::new());
            forest_heights[index].reserve(width);

            forest_visibility.push(vec![false; width]);
            for tree_index in 0..width {
                let digit = &line[tree_index..tree_index + 1];
                forest_heights[index].push(digit.parse().unwrap());
            }
            height += 1;
        }
    }

    // Part One
    let mut col_height_top: Vec<usize> = forest_heights[0].clone();
    let mut col_height_bottom: Vec<usize> = forest_heights[height - 1].clone();
    for rid in 0..width {
        let mut row_height_left = forest_heights[rid][0];
        let mut row_height_right = forest_heights[height - 1 - rid][width - 1];
        for cid in 0..height {
            let my_height_left_top = forest_heights[rid][cid];
            if row_height_left < my_height_left_top {
                row_height_left = my_height_left_top;
                forest_visibility[rid][cid] = true;
            }
            if col_height_top[cid] < my_height_left_top {
                col_height_top[cid] = my_height_left_top;
                forest_visibility[rid][cid] = true;
            }

            let my_height_right_bottom = forest_heights[height - 1 - rid][width - 1 - cid];
            if row_height_right < my_height_right_bottom {
                row_height_right = my_height_right_bottom;
                forest_visibility[height - 1 - rid][width - 1 - cid] = true;
            }
            if col_height_bottom[height - 1 - cid] < my_height_right_bottom {
                col_height_bottom[height - 1 - cid] = my_height_right_bottom;
                forest_visibility[height - 1 - rid][width - 1 - cid] = true;
            }
            if rid == 0 || cid == 0 {
                forest_visibility[rid][cid] = true;
                forest_visibility[height - 1 - rid][width - 1 - cid] = true;
            }
        }
    }

    let mut count = 0;
    for rid in 0..width {
        for cid in 0..height {
            //print!{"{}", if forest_visibility[rid][cid] {"1"} else {"0"}};
            if forest_visibility[rid][cid] {
                count += 1;
            }
        }
        //println!{};
    }

    //Part Two
    let mut max_score = 0;
    for rid in 1..(width - 1) {
        for cid in 1..(height - 1) {
            let mut left_visibility = 0;
            for lrid in (0..rid).rev() {
                left_visibility += 1;
                if forest_heights[rid][cid] <= forest_heights[lrid][cid] {
                    break;
                }
            }
            let mut right_visibility = 0;
            for rrid in (rid + 1)..height {
                right_visibility += 1;
                if forest_heights[rid][cid] <= forest_heights[rrid][cid] {
                    break;
                }
            }
            let mut top_visibility = 0;
            for tcid in (0..cid).rev() {
                top_visibility += 1;
                if forest_heights[rid][cid] <= forest_heights[rid][tcid] {
                    break;
                }
            }
            let mut bottom_visibility = 0;
            for bcid in (cid + 1)..width {
                bottom_visibility += 1;
                if forest_heights[rid][cid] <= forest_heights[rid][bcid] {
                    break;
                }
            }

            let score = left_visibility * right_visibility * top_visibility * bottom_visibility;
            if max_score < score {
                max_score = score;
            }
        }
    }

    println! {"{count}"};
    println! {"{max_score}"};
}
