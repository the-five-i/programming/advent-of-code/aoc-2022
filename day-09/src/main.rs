use std::fs::File;
use std::io::{self, BufRead};
use std::collections::HashSet;

#[derive(Hash,Clone,Copy)]
struct Point(i32, i32);

impl PartialEq for Point {
    fn eq(&self, other: &Self) -> bool {
        self.0 == other.0 && self.1 == other.1
    }
}
impl Eq for Point {}

impl Point {
    fn add(&mut self, other: Point) {
        self.0 += other.0;
        self.1 += other.1;
    }
}

enum Direction {
    Right,
    Left,
    Up,
    Down,
}


fn follow_move(point1 : &Point, point2 : &Point) -> Point {
    let distance_x = point1.0 - point2.0;
    let distance_y = point1.1 - point2.1;

    // Now its normalized, remove the division and substract 1 to the biggest to follow any
    // distance.
    if distance_x.abs() >= 2 || distance_y.abs() >= 2 {
        let follow_x = if distance_x == 0 { 0 } else { distance_x / distance_x.abs() };
        let follow_y = if distance_y == 0 { 0 } else { distance_y / distance_y.abs() };
        Point(follow_x, follow_y)
    } else {
        Point(0,0)
    }
}

fn apply_movements(direction: Direction, times: i32, knots: &mut Vec<Point>, tail_positions: &mut Vec<HashSet<Point>>) {
    use Direction::*;
    for _ in 0..times {
        match direction {
            Up => knots[0].add(Point(0,1)),
            Down => knots[0].add(Point(0,-1)),
            Right => knots[0].add(Point(1,0)),
            Left => knots[0].add(Point(-1,0)),
        };

        for i in 1..knots.len() {
            let tail_displacement = follow_move(&knots[i-1], &knots[i]);
            knots[i].add(tail_displacement);
            tail_positions[i].insert(knots[i]);
        }
    }
}

fn main() {
    let file = File::open("./moves.txt").unwrap();
    let lines = io::BufReader::new(file).lines();

    let mut knots : Vec<Point> = vec![Point(0,0); 10];
    let mut tail_positions : Vec<HashSet<Point>> = vec![HashSet::from([Point(0,0)]); 10];

    for line in lines {
        if let Ok(line) = line {
            let movement : Vec<_> = line.split(|sep| sep == ' ').collect();
            let (direction, times) : (_, i32) = match movement[..] {
                ["U", times] => (Direction::Up, times.parse().unwrap()),
                ["D", times] => (Direction::Down, times.parse().unwrap()),
                ["L", times] => (Direction::Left, times.parse().unwrap()),
                ["R", times] => (Direction::Right, times.parse().unwrap()),
                _ => panic!{"Parsing error, unknwon command: {}", line},
            };

            apply_movements(direction, times, &mut knots, &mut tail_positions);
        }
    }

    println!{"{}", tail_positions[1].len()};
    println!{"{}", tail_positions[9].len()};
}
