use std::fs::File;
use std::io::{self,BufRead};

enum InstructionType {
    Addx(i32),
    Noop,
}

struct Instruction {
    cycles: i32,
    instruction: InstructionType,
}


fn parse_instruction(instruction: &str) -> Option<Instruction> {

    let mut instruction = instruction.split(|sep| sep == ' ');

    match instruction.next().unwrap() {
        "addx" => {
            let value: i32 = instruction.next().unwrap().parse().unwrap();
            Some(Instruction{cycles: 2, instruction: InstructionType::Addx(value)})
        },
        "noop" => {
            Some(Instruction{cycles: 1, instruction: InstructionType::Noop})
        },
        _ => None,
    }
}

fn execute_instruction(instruction: &mut Instruction, cycles: &mut i32, register: &mut i32) -> i32 {
    let mut signal_strength = 0;

    loop {

        let CRT_cursor = (*cycles - 1) % 40;

        if CRT_cursor >= (*register -1) && CRT_cursor <= (*register +1) {
            print!{"#"};
        } else {
            print!{"."};
        }

        if CRT_cursor == 39 {
            println!{};
        }


        if (*cycles - 20) % 40 == 0 {
            signal_strength += *cycles * *register;
        }

        *cycles += 1;
        instruction.cycles -= 1;

        if instruction.cycles == 0 {
            if let InstructionType::Addx(value) = instruction.instruction {
                *register += value;
            }
            break;
        }

    }

    signal_strength
}

fn main() {
    let file = File::open("./program.txt").unwrap();
    let lines : Vec<_> = io::BufReader::new(file).lines().collect();

    let mut cycles = 1;
    let mut register = 1;
    let mut signal_strength = 0;

    for line in lines {
        if let Ok(line) = line {
            let mut instruction = parse_instruction(&line).unwrap();

            signal_strength += execute_instruction(&mut instruction, &mut cycles, &mut register);
        }
    }

    println!{"{signal_strength}"};

}
