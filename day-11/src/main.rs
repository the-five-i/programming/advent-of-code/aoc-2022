use std::collections::LinkedList;
use std::fs::File;
use std::io::{self, BufRead};

#[derive(Debug)]
enum OperationType {
    Sum,
    Prod,
}

enum Value {
    Old,
    Immediate(i32),
}

impl Value {
    fn unwrap_or_else(&self, other: i32) -> i32 {
        use Value::*;
        match self {
            Old => other,
            Immediate(value) => *value,
        }
    }
}

struct Operation {
    func: OperationType,
    lhs: Value,
    rhs: Value,
}

impl Operation {
    fn from(func: OperationType, lhs: Value, rhs: Value) -> Self {
        Operation { func, lhs, rhs }
    }

    fn operate(&self, old: i32) -> i32 {
        use OperationType::*;

        let lhs = self.lhs.unwrap_or_else(old);
        let rhs = self.rhs.unwrap_or_else(old);

        //println!{"{} {:?} {}", lhs, self.func, rhs};
        match self.func {
            Sum => lhs + rhs,
            Prod => lhs * rhs,
        }
    }
}

struct Monkey {
    items: LinkedList<i32>,
    item_count: usize,
    operation: Operation,
    test: i32,
    if_true: usize,
    if_false: usize,
}

impl Monkey {
    fn fmt(&self) -> String {
        format! {"{:?}", self.items}
    }

    fn process_item(&mut self) -> Option<(i32, usize)> {
        let item = self.items.pop_front();

        if let Some(item) = item {
            self.item_count += 1;
            let worry_level = self.operation.operate(item);
            let worry_level = worry_level / 3;
            if worry_level % self.test == 0 {
                Some((worry_level, self.if_true))
            } else {
                Some((worry_level, self.if_false))
            }
        } else {
            None
        }
    }

    fn add_item(&mut self, worry_level: i32) {
        self.items.push_back(worry_level);
    }

    fn from(
        items: LinkedList<i32>,
        operation: Operation,
        test: i32,
        if_true: usize,
        if_false: usize,
    ) -> Monkey {
        Monkey {
            items: items,
            item_count: 0,
            operation: operation,
            test: test,
            if_true: if_true,
            if_false: if_false,
        }
    }
}

fn main() {
    //let mut monkeys: Vec<Monkey> = real_input();
    let mut monkeys: Vec<Monkey> = test_input();

    for round in 0..20 {
        //for monkey in &mut monkeys {
        for index in 0..monkeys.len() {
            loop {
                if let Some((worry_level, reciver)) = monkeys[index].process_item() {
                    monkeys[reciver].add_item(worry_level);
                } else {
                    break;
                }
            }
        }
        println! {"Round: {}", round};
        for index in 0..monkeys.len() {
            println! {"Monkey {index}: {}", monkeys[index].fmt()};
        }
        println!{"{:?}", monkeys
            .iter()
            .map(|monkey| monkey.item_count)
            .collect::<Vec<_>>()};
        println!{};
    }

    let mut monkeys = monkeys
        .iter()
        .map(|monkey| monkey.item_count)
        .collect::<Vec<_>>();
    println! {"{:?}", monkeys};
    monkeys.sort();
    let business: usize = monkeys.iter().rev().take(2).product();

    println! {"{business}"};
}

fn test_input() -> Vec<Monkey> {
    use OperationType::*;
    use Value::*;
    vec![
        Monkey::from(
            LinkedList::from([79, 98]),
            Operation::from(Prod, Old, Immediate(19)),
            23,
            2,
            3,
        ),
        Monkey::from(
            LinkedList::from([54, 65, 75, 74]),
            Operation::from(Sum, Old, Immediate(6)),
            19,
            2,
            0,
        ),
        Monkey::from(
            LinkedList::from([79, 60, 97]),
            Operation::from(Prod, Old, Old),
            13,
            1,
            3,
        ),
        Monkey::from(
            LinkedList::from([74]),
            Operation::from(Sum, Old, Immediate(3)),
            17,
            0,
            1,
        ),
    ]
}

fn real_input() -> Vec<Monkey> {
    use Value::*;
    use OperationType::*;
    vec![
        Monkey::from(
            LinkedList::from([89, 95, 92, 64, 87, 68]),
            Operation::from(Prod, Old, Immediate(11)),
            2,
            7,
            4,
        ),
        Monkey::from(
            LinkedList::from([87, 67]),
            Operation::from(Sum, Old, Immediate(1)),
            13,
            3,
            6,
        ),
        Monkey::from(
            LinkedList::from([95, 79, 92, 82, 60]),
            Operation::from(Sum, Old, Immediate(6)),
            3,
            1,
            6,
        ),
        Monkey::from(
            LinkedList::from([67, 97, 56]),
            Operation::from(Prod, Old, Old),
            17,
            7,
            0,
        ),
        Monkey::from(
            LinkedList::from([80, 68, 87, 94, 61, 59, 50, 68]),
            Operation::from(Prod, Old, Immediate(7)),
            19,
            5,
            2,
        ),
        Monkey::from(
            LinkedList::from([73, 51, 76, 59]),
            Operation::from(Sum, Old, Immediate(8)),
            7,
            2,
            1,
        ),
        Monkey::from(
            LinkedList::from([92]),
            Operation::from(Sum, Old, Immediate(5)),
            11,
            3,
            0,
        ),
        Monkey::from(
            LinkedList::from([99, 76, 78, 76, 79, 90, 89]),
            Operation::from(Sum, Old, Immediate(7)),
            5,
            4,
            5,
        ),
    ]
}
