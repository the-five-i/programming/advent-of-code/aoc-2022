use std::fs::File;
use std::io::{self, BufRead};
use std::collections::LinkedList;

#[derive(Debug, Clone, Copy, PartialEq)]
struct Point(usize, usize);

impl Point {
    fn right(&self) -> Option<Self> {
        Some(Point(self.0, self.1 + 1))
    }
    fn left(&self) -> Option<Self> {
        if self.1 == 0 {
            return None;
        }
        Some(Point(self.0, self.1 - 1))
    }
    fn up(&self) -> Option<Self> {
        Some(Point(self.0 + 1, self.1))
    }
    fn down(&self) -> Option<Self> {
        if self.0 == 0 {
            return None;
        }
        Some(Point(self.0 - 1, self.1))
    }
}

trait PointAccess {
    fn at(&self, point: Option<Point>) -> Option<usize>;
}

type Grid = Vec<Vec<usize>>;

impl PointAccess for Grid {
    fn at(&self, point: Option<Point>) -> Option<usize> {
        let point = point?;

        if point.0 >= self.len() {
            return None;
        }
        if point.1 >= self[point.0].len() {
            return None;
        }

        Some(self[point.0][point.1])
    }
}

fn main() {
    let file = File::open("./grid.txt").unwrap();
    let lines: Vec<_> = io::BufReader::new(file).lines().collect();

    let mut grid: Grid = Vec::new();

    let mut start: Option<Point> = None;
    let mut end: Option<Point> = None;

    let mut width = None;
    let mut height = None;

    for (row, line) in lines.iter().enumerate() {
        if let Ok(line) = line {
            let mut col_start = None;
            let mut col_end = None;
            width = Some(line.len());
            grid.push(
                line.chars()
                    .enumerate()
                    .map(|value| match value {
                        (i, 'S') => {
                            col_start = Some(i);
                            usize::try_from('a'.to_digit(36).unwrap()).unwrap()
                        }
                        (i, 'E') => {
                            col_end = Some(i);
                            usize::try_from('z'.to_digit(36).unwrap()).unwrap()
                        }
                        (_, other) => {
                            usize::try_from(other.to_digit(36).unwrap()).unwrap()
                        }
                    })
                    .collect(),
            );

            if let Some(col_start) = col_start {
                start = Some(Point(row, col_start));
            }
            if let Some(col_end) = col_end {
                end = Some(Point(row, col_end));
            }
            height = match height {
                None => Some(1),
                Some(v) => Some(v + 1),
            };
        }
    }

    let start = start.unwrap();
    let end = end.unwrap();

    // Part one
    let count = path_length(&grid, start, end);
    let count = count.unwrap();
    println! {"{count}"};

    // Part two
    let mut count : Option<usize> = None;
    for row in 0..grid.len() {
        for col in 0..grid[row].len() {
            if grid[row][col] == 10 {
                let curr_count = path_length(&grid, Point(row, col), end);
                count = match (count, curr_count) {
                    (Some(a), Some(b)) if a > b => curr_count,
                    (None, min) => min,
                    _ => count,
                }
            }
        }
    }
    let count = count.unwrap();
    println! {"{count}"};

}

fn path_length( heights: &Vec<Vec<usize>>, start: Point, end: Point) -> Option<usize> {
    let height = heights.len();

    if height == 0 {
        panic!{"Grid has heigth 0."};
    }

    let width = heights[0].len();

    if width == 0 {
        panic!{"Grid has width 0."};
    }

    let mut visited : Vec<Vec<bool>> = vec![vec![false; width]; height];
    let mut pending: LinkedList<(usize, Point)> = LinkedList::new();

    pending.push_back((0,start));

    loop {

        if let Some((length, current)) = pending.pop_front() {
            if current == end {
                return Some(length);
            }

            let current_height = heights[current.0][current.1];

            for next in [current.up(), current.down(), current.right(), current.left()] {
                if let Some(next_height) = heights.at(next) {
                    if current_height + 1 >= next_height {
                        if let Some(next_point) = next {
                        if !visited[next_point.0][next_point.1] {
                            pending.push_back((length+1, next_point));
                            visited[next_point.0][next_point.1] = true;
                        }
                        }
                    }
                }
            }
        }

        if pending.len() == 0 {
            return None;
        }
    };
}
