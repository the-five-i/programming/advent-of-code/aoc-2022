use std::cmp::Ordering::{self, *};
use std::fs;

#[derive(Clone)]
enum Element {
    Int(isize),
    List(Vec<Element>),
}

use Element::*;

fn compare_lists(lhs: &Vec<Element>, rhs: &Vec<Element>) -> Ordering {
    let mut lhs = lhs.iter();
    let mut rhs = rhs.iter();
    loop {
        match (lhs.next(), rhs.next()) {
            (None, None) => return Equal,
            (None, Some(_)) => return Less,
            (Some(_), None) => return Greater,
            (Some(lhs), Some(rhs)) => {
                let compared = lhs.cmp(rhs);
                if compared == Equal {
                    continue;
                }
                return compared;
            }
        }
    }
}

impl Element {
    fn cmp(self: &Element, other: &Element) -> Ordering {
        match (self, other) {
            (Int(lhs), Int(rhs)) => lhs.cmp(&rhs),
            (lhs @ Int(_), rhs @ List(_)) => List(vec![lhs.clone()]).cmp(rhs),
            (lhs @ List(_), rhs @ Int(_)) => lhs.cmp(&List(vec![rhs.clone()])),
            (List(lhs), List(rhs)) => compare_lists(lhs, rhs),
        }
    }
}

// S -> I
//   -> [ L ]

fn parse_s(index: &mut usize, s: &str) -> Option<Element> {
    let parsed = match &s[*index..=*index] {
        // First of S
        "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9" => Some(Int(parse_i(index, s)?)),
        "[" => {
            *index += 1;

            let list = parse_l(index, s);

            if &s[*index..=*index] != "]" {
                panic! {"Syntax error."};
            }
            *index += 1;

            Some(List(list?))
        }
        _ => None,
    };

    parsed
}

// L -> S,L
//   -> S

fn parse_l(index: &mut usize, s: &str) -> Option<Vec<Element>> {
    let mut list = vec![];

    if &s[*index..=*index] == "]" {
        return Some(list);
    }

    list.push(parse_s(index, s)?); // Consume & append S

    if &s[*index..=*index] == "," {
        *index += 1; // Consume ","
        list.append(&mut parse_l(index, s)?); // Consume & append L
    }

    Some(list)
}

// I -> d
//   -> dI

// d -> 0
//   -> 1
//   -> 2
//   -> 3
//   -> 4
//   -> 5
//   -> 6
//   -> 7
//   -> 8
//   -> 9

fn parse_i(index: &mut usize, s: &str) -> Option<isize> {
    let mut integer = String::from("");

    loop {
        match &s[*index..=*index] {
            digit @ ("0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9") => {
                integer.push_str(digit)
            }
            _ => break,
        }
        *index += 1;
    }

    if integer.len() == 0 {
        return None;
    }

    integer.parse::<isize>().ok()
}

fn main() {
    let contents = fs::read_to_string("./lists.txt").expect("Could not read file");

    // Part One
    let mut sum = 0;
    let mut index = 1;
    let mut lines = contents.lines();
    loop {
        let line1 = lines.next();
        if None == line1 {
            break;
        }
        let mut i = 0;
        let line1 = parse_s(&mut i, line1.unwrap()).unwrap();

        let line2 = lines.next();
        if None == line2 {
            break;
        }
        let mut i = 0;
        let line2 = parse_s(&mut i, line2.unwrap()).unwrap();

        if ((&line1).cmp(&line2)) != Greater {
            sum += index;
        }

        lines.next(); // Skip empty line
        index += 1;
    }
    println! {"{sum}"};

    // Part Two
    let mut packets: Vec<Element> = vec![];
    let mut lines = contents.lines();

    // Parse the input again
    loop {
        let line = lines.next();
        if None == line {
            break;
        }
        let line = line.unwrap();
        if line == "" {
            continue;
        }
        let mut i = 0;
        let line = parse_s(&mut i, line).unwrap();

        packets.push(line);
    }

    // Add dividers to the set
    let divider_1 = List(vec![List(vec![Int(2)])]);
    let divider_2 = List(vec![List(vec![Int(6)])]);
    packets.push(divider_1.clone());
    packets.push(divider_2.clone());

    // Sort packets
    packets.sort_by(|a, b| a.cmp(b));

    let mut divider_1_value = 0;
    let mut divider_2_value = 0;
    for (index, packet) in packets.iter().enumerate() {
        if packet.cmp(&divider_1) == Equal {
            divider_1_value = index + 1;
            continue;
        }
        if packet.cmp(&divider_2) == Equal {
            divider_2_value = index + 1;
            continue;
        }
    }
    println! {"{}", divider_1_value * divider_2_value};
}
