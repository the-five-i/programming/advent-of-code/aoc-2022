use std::cmp::{max, min};
use std::collections::HashMap;
use std::env;
use std::fs;

use std::ops::Add;

#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
struct Point(i32, i32);

impl Add for Point {
    type Output = Self;

    fn add(self, other: Self) -> Self::Output {
        Point(self.0 + other.0, self.1 + other.1)
    }
}

enum Element {
    Sand,
    Rock,
}

struct Config {
    min: Point,
    max: Point,
    elements: HashMap<Point, Element>,
}

fn main() {
    // Parse arguments
    let input_file = match env::args().skip(1).next() {
        Some(s) if s == "small" => "scan.small.txt",
        _ => "scan.txt",
    };

    let contents = fs::read_to_string(input_file).expect("Error reading file");
    let Config {
        min,
        max,
        mut elements,
    } = parse_input(contents).expect("Error parsing the input file!");

    println! {"Min: {min:?}"};
    println! {"Max: {max:?}"};

    let start = Point(500, 0);
    print_cave(start, min, max, &elements);

    // Part One
    compute_sand_simulation(start, min, max, &mut elements);

    print_cave(start, min, Point(max.0, max.1+2), &elements);

    let sand_elements = elements.values().fold(0, |sum, elem| match *elem {
        Element::Sand => sum + 1,
        _ => sum,
    });

    println!{"{sand_elements}"};

    // Part Two
    compute_sand_simulation_with_bottom(start, min, max, &mut elements);

    let max = elements.keys().fold(Point(0, 0), |max, curr| {
        Point(
            if curr.0 > max.0 { curr.0 } else { max.0 },
            if curr.1 > max.1 { curr.1 } else { max.1 },
        )
    });

    let min = elements.keys().fold(Point(max.0, 0), |min, curr| {
        Point(
            if curr.0 < min.0 { curr.0 } else { min.0 },
            if curr.1 < min.1 { curr.1 } else { min.1 },
        )
    });

    print_cave(start, min, max, &elements);

    let sand_elements = elements.values().fold(0, |sum, elem| match *elem {
        Element::Sand => sum + 1,
        _ => sum,
    });


    println!{"{sand_elements}"};
}

fn compute_sand_simulation(
    start: Point,
    min: Point,
    max: Point,
    elements: &mut HashMap<Point, Element>,
) {
    let mut sand = start;
    loop {
        if sand.1 > max.1 || sand.0 < min.0 || sand.0 > max.0 {
            break;
        }

        let next = sand + Point(0, 1);
        if !elements.contains_key(&next) {
            sand = next;
            continue;
        }

        let next = sand + Point(-1, 1);
        if !elements.contains_key(&next) {
            sand = next;
            continue;
        }

        let next = sand + Point(1, 1);
        if !elements.contains_key(&next) {
            sand = next;
            continue;
        }

        elements.insert(sand, Element::Sand);
        sand = start;
    }
}

fn compute_sand_simulation_with_bottom(
    start: Point,
    min: Point,
    max: Point,
    elements: &mut HashMap<Point, Element>,
) {
    let mut sand = start;
    loop {


        let next = sand + Point(0, 1);
        if next.1 > max.1 + 1 {
            elements.insert(sand, Element::Sand);
            sand = start;
            continue;
        }

        if !elements.contains_key(&next) {
            sand = next;
            continue;
        }

        let next = sand + Point(-1, 1);
        if !elements.contains_key(&next) {
            sand = next;
            continue;
        }

        let next = sand + Point(1, 1);
        if !elements.contains_key(&next) {
            sand = next;
            continue;
        }


        elements.insert(sand, Element::Sand);

        if sand == start {
            break;
        }

        sand = start;
    }
}

fn parse_input(contents: String) -> Option<Config> {
    let mut elements = HashMap::new();

    for line in contents.lines() {
        let mut rock_segment = Vec::new();
        for raw_point in line.split(" -> ") {
            let mut raw_point = raw_point.split(",");
            let point = match (raw_point.next(), raw_point.next(), raw_point.next()) {
                (Some(x), Some(y), None) => Point(x.parse().ok()?, y.parse().ok()?),
                _ => panic! {"Parsing error!"},
            };

            rock_segment.push(point);
        }

        for point_pair in rock_segment.windows(2) {
            match point_pair {
                [Point(x1, y1), Point(x2, y2)] if x1 != x2 && y1 != y2 => {
                    panic! {"Error diagonal line"}
                }
                [Point(x1, y), Point(x2, _)] if x1 != x2 => {
                    let y = *y;
                    let a = *min(x1, x2);
                    let b = *max(x1, x2);
                    for x in a..=b {
                        elements.insert(Point(x, y), Element::Rock);
                    }
                }
                [Point(x, y1), Point(_, y2)] if y1 != y2 => {
                    let x = *x;
                    let a = *min(y1, y2);
                    let b = *max(y1, y2);
                    for y in a..=b {
                        elements.insert(Point(x, y), Element::Rock);
                    }
                }
                _ => (),
            }
        }
    }

    let max = elements.keys().fold(Point(0, 0), |max, curr| {
        Point(
            if curr.0 > max.0 { curr.0 } else { max.0 },
            if curr.1 > max.1 { curr.1 } else { max.1 },
        )
    });

    let min = elements.keys().fold(Point(max.0, 0), |min, curr| {
        Point(
            if curr.0 < min.0 { curr.0 } else { min.0 },
            if curr.1 < min.1 { curr.1 } else { min.1 },
        )
    });

    Some(Config { min, max, elements })
}

fn print_cave(current: Point, min: Point, max: Point, elements: &HashMap<Point, Element>) {
    for y in min.1..=max.1 {
        for x in min.0..=max.0 {
            if Point(x, y) == current {
                print! {"+"};
                continue;
            }
            if let Some(element) = elements.get(&Point(x, y)) {
                match element {
                    Element::Sand => print! {"o"},
                    Element::Rock => print! {"#"},
                }
            } else {
                print! {"."};
            }
        }
        println! {};
    }
}
