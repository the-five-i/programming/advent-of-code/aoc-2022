use std::env;
use std::fs;

use std::collections::HashMap;

use std::cmp::max;
use std::ops::{Add, Sub};

#[derive(Debug, Hash, Eq, PartialEq, Clone, Copy)]
struct Point(i64, i64);

impl Add for Point {
    type Output = Self;
    fn add(self: Self, other: Self) -> Self {
        Point(self.0 + other.0, self.1 + other.1)
    }
}

impl Sub for Point {
    type Output = Self;
    fn sub(self: Self, other: Self) -> Self {
        Point(self.0 - other.0, self.1 - other.1)
    }
}

#[derive(Clone, Copy, Eq, PartialEq)]
enum Element {
    Free,
    Sensor,
    Beacon,
}

#[derive(Clone)]
struct Config {
    elements: Vec<(Point, Point)>,
}

fn main() {
    let input_file = match env::args().skip(1).next() {
        Some(s) if s == "small" => "sensors.small.txt",
        _ => "sensors.txt",
    };

    let contents = fs::read_to_string(input_file).expect("Error reading file");
    let config = parse_input(contents).expect("Error parsing the input file!");
    let non_beacon = find_non_beacon(config.clone());
    println! {"{non_beacon}"};

    let tuning_frequency =
        find_tuning_frequency(config.clone()).expect("Could not find tuning frequency!");
    println! {"{tuning_frequency}"};
}

impl Point {
    fn distance(lhs: Point, rhs: Point) -> i64 {
        i64::abs(lhs.0 - rhs.0) + i64::abs(lhs.1 - rhs.1)
    }
}

fn find_non_beacon(config: Config) -> i64 {
    let row = 2000000;

    let mut space: HashMap<Point, Element> = HashMap::new();

    for (sensor, beacon) in config.elements {
        space.insert(sensor, Element::Sensor);
        space.insert(beacon, Element::Beacon);

        let distance_to_row = Point::distance(sensor, Point(sensor.0, row));
        let distance_to_beacon = Point::distance(sensor, beacon);
        let offset = max(0, distance_to_beacon - distance_to_row);

        for offset in 0..=offset {
            let offset_point = Point(sensor.0 - offset, row);
            if !space.contains_key(&offset_point) {
                space.insert(offset_point, Element::Free);
            }

            let offset_point = Point(sensor.0 + offset, row);
            if !space.contains_key(&offset_point) {
                space.insert(offset_point, Element::Free);
            }
        }
    }

    space
        .iter()
        .filter(|(point, _)| point.1 == row)
        .fold(0, |sum, (_, elem)| {
            sum + if *elem == Element::Free { 1 } else { 0 }
        })
}

fn find_tuning_frequency(config: Config) -> Option<i64> {
    let limit_min = 0;
    let limit_max = 4000000;

    for x in limit_min..=limit_max {
        let mut y = limit_min;
        loop {
            let mut free = true;
            for (sensor, beacon) in &config.elements {
                let distance_to_beacon = Point::distance(*sensor, *beacon);
                let distance_to_point = Point::distance(*sensor, Point(x, y));

                if distance_to_beacon >= distance_to_point {
                    y += distance_to_beacon - distance_to_point;
                    free = false;
                    break;
                }
            }

            if free {
                return Some(x * 4000000 + y);
            }

            y += 1;
            if y > limit_max {
                break;
            }
        }
    }
    None
}

fn parse_input(contents: String) -> Option<Config> {
    let mut elements = Vec::new();

    for line in contents.lines() {
        let parsed: Vec<i64> = line
            .split("=")
            .skip(1)
            .map(|elem| {
                elem.split(|sep| sep == ',' || sep == ':')
                    .next()
                    .unwrap()
                    .parse()
                    .unwrap()
            })
            .collect();
        let sensor = Point(*parsed.get(0)?, *parsed.get(1)?);
        let beacon = Point(*parsed.get(2)?, *parsed.get(3)?);

        elements.push((sensor, beacon));
    }

    Some(Config { elements })
}
