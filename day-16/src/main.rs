use std::cmp::max;
use std::collections::{HashMap, HashSet};
use std::env;
use std::fs;
use std::str::FromStr;

use lazy_static::lazy_static;
use regex::Regex;

struct Room {
    id: usize,
    flow_rate: usize,
    connections: Vec<usize>,
}

fn main() {
    let input_file = match env::args().skip(1).next() {
        Some(s) if s == "small" => "valve_rooms.small.txt",
        _ => "valve_rooms.txt",
    };

    let contents = fs::read_to_string(input_file).expect("Error reading file");
    let (flow_rates, adjacencies) = parse_line(contents);
    if let Some(flow_rate) = find_optimal_path(flow_rates, adjacencies) {
        println! {"{flow_rate}"};
    }
}

struct Node {
    id: usize,
    minutes: usize,
    flow_rate: usize,
}

fn find_optimal_path(flow_rates: Vec<usize>, adjacencies: Vec<Vec<Option<usize>>>) -> Option<usize> {
    let mut next_node = vec![(Node{id:0, minutes: 30, flow_rate: 0}, Vec::new())];

    let mut max_flow_rate = 0;

    loop {
        let current = next_node.pop();
        if current.is_none() {
            break;
        }
        let (current, path) = current.unwrap();

        max_flow_rate = max(max_flow_rate, current.flow_rate);

        if current.minutes == 0 {
            continue;
        }

        for next in 0..adjacencies[current.id].len() {
            if path.contains(&next) {
                continue;
            }
            if let Some(cost) = adjacencies[current.id][next] {
                let next_minutes = current.minutes.saturating_sub(cost + 1);
                let next_valve_contribution = flow_rates[next] * next_minutes;
                if 0 ==  next_valve_contribution {
                    continue;
                }
                let next_flow_rate = current.flow_rate + next_valve_contribution;
                let mut next_path = path.clone();
                next_path.push(next);
                next_node.push((Node{
                    id: next,
                    minutes: next_minutes,
                    flow_rate: next_flow_rate,
                }, next_path));
            }
        }
    }

    Some(max_flow_rate)
}

fn parse_line(s: String) -> (Vec<usize>, Vec<Vec<Option<usize>>>) {
    // Sensor at x=2, y=18: closest beacon is at x=-2, y=15
    lazy_static! {
        static ref RE: Regex = Regex::new(
            r"Valve ([A-Z]{2}) has flow rate=(\d+); tunnels? leads? to valves? ((?:[A-Z]{2}, )*(?:[A-Z]{2}))"
        )
        .unwrap();
    }

    let mut current_index: usize = 0;
    let mut indexes: HashMap<String, usize> = HashMap::new();
    let mut rooms: Vec<Room> = vec![];

    get_room_id(&mut current_index, &mut indexes, "AA".to_string());
    for line in s.lines() {
        let caps = RE.captures(&line).unwrap();

        let current_valve = &caps[1].to_string();
        let id = get_room_id(&mut current_index, &mut indexes, current_valve.to_string());

        let flow_rate = usize::from_str(&caps[2]).unwrap();

        let named_connections = &caps[3];

        // Parse as a whole and split!
        let mut connections = Vec::new();
        for connection in named_connections.split(", ") {
            let connection = get_room_id(&mut current_index, &mut indexes, connection.to_string());
            connections.push(connection);
        }

        rooms.push(Room {
            id,
            flow_rate,
            connections,
        });
    }

    let mut flow_rates: Vec<usize> = vec![0; rooms.len()];
    let mut adjacencies: Vec<Vec<Option<usize>>> = vec![vec![None; current_index]; current_index];
    for room in &rooms {
        flow_rates[room.id] = room.flow_rate;
        for connection in &room.connections {
            adjacencies[room.id][*connection] = Some(1);
        }
    }

    for from in 0..adjacencies.len() {
        for to in 0..adjacencies[from].len() {
            if from == to {
                continue;
            }

            if flow_rates[from] == 0 {
                continue;
            }

            let symetry_check = match (adjacencies[from][to], adjacencies[to][from]) {
                (Some(a), Some(b)) if a != b => panic! {"Symetry error"},
                (Some(a), _) => Some(a),
                (_, Some(b)) => Some(b),
                (None, None) => None,
            };

            if None == symetry_check {
                let cost = find_path_cost(&adjacencies, from, to)
                    .expect("Could not find path from {from} to {to}.");
                adjacencies[from][to] = Some(cost);
                adjacencies[to][from] = Some(cost);
            } else {
                adjacencies[from][to] = symetry_check;
                adjacencies[to][from] = symetry_check;
            }
        }
    }


    // Discard connections with nodes that have 0 flow rate.
    for from in 0..adjacencies.len() {
        for to in 0..adjacencies[from].len() {
            if flow_rates[to] == 0 {
                adjacencies[from][to] = None;
            }
        }
    }

    (flow_rates, adjacencies)
}

fn get_room_id(
    curr_index: &mut usize,
    indexes: &mut HashMap<String, usize>,
    name: String,
) -> usize {
    if indexes.contains_key(&name) {
        *indexes.get(&name).unwrap()
    } else {
        let valve_id = *curr_index;
        indexes.insert(name, *curr_index);
        *curr_index += 1;
        valve_id
    }
}

fn find_path_cost(adjacencies: &Vec<Vec<Option<usize>>>, from: usize, to: usize) -> Option<usize> {
    let mut next_node = vec![(from, 0)];
    let mut visited = HashSet::new();
    visited.insert(from);

    loop {

        if let Some((current, length)) = next_node.pop() {
            if current == to {
                return Some(length);
            }

            visited.insert(current);

            for next in 0..adjacencies[current].len() {
                if visited.contains(&next) {
                    continue;
                }
                if let Some(1) = adjacencies[current][next] {
                    next_node.push((next, length + 1));
                }
            }
        } else {
            break;
        }

        next_node.sort_by(|(_, lhs), (_, rhs)| rhs.cmp(lhs));
    }

    None
}
