use std::fs;
use std::collections::HashSet;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Point {
    x:isize,
    y:isize,
}

impl std::ops::Add for Point {
    type Output = Self;

    fn add(self, rhs: Point) -> Self::Output {
        Point{
            x: self.x + rhs.x,
            y: self.y + rhs.y
        }
    }
}

type Jets = Vec<Direction>;

#[derive(Clone,Copy)]
pub enum Direction {
    Left,
    Right,
}

#[derive(Clone,Copy)]
pub enum Rock {
    Horizontal,
    Cros,
    Elbow,
    Vertical,
    Square,
}

impl Rock {
    fn get_points(&self, point: Point) -> Vec<Point> {
        use Rock::*;
        match *self {
            Horizontal => vec![
                point,
                point + Point{x:1, y:0},
                point + Point{x:2, y:0},
                point + Point{x:3, y:0}],
            Cros => vec![
                point + Point{x:1, y:0},
                point + Point{x:0, y:1},
                point + Point{x:1, y:1},
                point + Point{x:2, y:1},
                point + Point{x:1, y:2}],
            Elbow => vec![
                point,
                point + Point{x:1, y:0},
                point + Point{x:2, y:0},
                point + Point{x:2, y:1},
                point + Point{x:2, y:2}],
            Vertical => vec![
                point,
                point + Point{x:0, y:1},
                point + Point{x:0, y:2},
                point + Point{x:0, y:3}],
            Square => vec![
                point,
                point + Point{x:1, y:0},
                point + Point{x:0, y:1},
                point + Point{x:1, y:1}],
        }
    }

    fn height(&self) -> usize {
        use Rock::*;
        match *self {
            Horizontal => 1,
            Cros => 3,
            Elbow => 3,
            Vertical => 4,
            Square => 2,
        }
    }
}

impl From<char> for Direction {
    fn from(value: char) -> Self {
        match value {
            '<' => Direction::Left,
            '>' => Direction::Right,
            _ => panic!{"Could not find direction from char: {value}"},
        }
    }
}

mod utils {
    use super::*;

    pub fn parse_input(contents: String) -> Jets {
        contents.chars().filter(|value| *value != '\n').map(Direction::from).collect()
    }

    fn jet_push(point: Point, direction: Direction) -> Point {
        match direction {
            Direction::Left => point + Point{x: -1, y: 0},
            Direction::Right => point + Point{x: 1, y: 0},
        }
    }

    fn fall_down(point: Point) -> Point {
        point + Point{x:0, y: -1}
    }

    fn check_position(chamber: &HashSet<Point>, rock: Rock, point: Point) -> bool {
        for point in rock.get_points(point) {
            if point.x < 0 || point.x >= 7 || point.y < 0 {
                return false;
            }

            if chamber.contains(&point) {
                return false;
            }
        }

        true
    }

    pub fn solve(jets: Jets, number_of_rocks: usize) -> Result<usize, String> {
        use Rock::*;

        let mut chamber : HashSet<Point> = HashSet::new();
        let mut heighest = 0;

        let rocks = vec![Horizontal, Cros, Elbow, Vertical, Square];

        let mut turn = 0;

        for rock in 0..number_of_rocks {
            let mut current = Point{ x: 2, y: heighest as isize + 3 };
            loop {
                let next = jet_push(current, jets[turn % jets.len()]);
                if check_position(&chamber,rocks[rock % rocks.len()],next) {
                    current = next;
                }
                turn += 1;

                let next = fall_down(current);
                if check_position(&chamber,rocks[rock % rocks.len()],next) {
                    current = next;
                } else {
                    break;
                }
            }

            let height = rocks[rock % rocks.len()].height() + current.y as usize;
            heighest = if height > heighest { height } else {heighest};
            for point in rocks[rock % rocks.len()].get_points(current) {
                chamber.insert(point);
            }
        }

        Ok(heighest)
    }
}

pub mod part_one {
    use super::*;
    pub fn solve(jets: Jets) -> Result<usize, String> {
        utils::solve(jets, 2022)
    }
}

pub mod part_two {
    use super::*;
    pub fn solve(jets: Jets) -> Result<usize, String> {
        utils::solve(jets, 1000000000000)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn part_one() -> Result<(), String> {
        let contents = fs::read_to_string("./jets_sample.txt").expect("Could not find file");
        let jets = utils::parse_input(contents);
        let result = part_one::solve(jets)?;
        println!{"Part One: {result}"};
        assert_eq!{result, 3068};
        Ok(())
    }

    #[test]
    fn part_two() -> Result<(), String> {
        let contents = fs::read_to_string("./jets_sample.txt").expect("Could not find file");
        let jets = utils::parse_input(contents);
        let result = part_two::solve(jets)?;
        println!{"Part Two: {result}"};
        assert_eq!{result, 1514285714288};
        Ok(())
    }
}

fn main() -> Result<(), String> {
        let contents = fs::read_to_string("./jets.txt").expect("Could not find file");
        let jets = utils::parse_input(contents);

        let result = part_one::solve(jets.clone())?;
        println!{"Part One: {result}"};

        let result = part_two::solve(jets)?;
        println!{"Part Two: {result}"};

        Ok(())
}
