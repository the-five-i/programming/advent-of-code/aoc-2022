use std::fs;
use std::iter::Peekable;

#[derive(PartialEq)]
pub enum Cell {
    Wall,
    Free,
    Empty,
}

#[derive(Debug)]
pub enum Move {
    Walk(usize),
    Turn(TurnDirection),
}

#[derive(Debug)]
pub enum TurnDirection {
    Clockwise,
    Counter,
}

pub struct Config {
    map: Vec<Vec<Cell>>,
    path: Vec<Move>,
}

fn main() {
    println!{"Run `cargo test`"};
}

fn parse_input(contents: String) -> Result<Config, String> {

    let mut map = Vec::new();

    let mut row_number = 0;

    for line in contents.lines() {
        if line == "" {
            break;
        }

        map.push(Vec::new());

        for cell_value in line.chars() {
            let cell = match cell_value {
                '#' => Ok(Cell::Wall),
                '.' => Ok(Cell::Free),
                ' ' => Ok(Cell::Empty),
                _ => Err(String::from("Unknown map character.")),
            }?;

            map[row_number].push(cell);
        }

        row_number += 1;
    }

    let path = contents.lines().last();
    let path = path.ok_or(String::from("Could not parse path."))?;
    let path = path.to_string();
    let path = parse_path(path.chars())?;

    Ok(Config{map, path})
}

fn parse_path(c: std::str::Chars) -> Result<Vec<Move>, String> {
    let mut c = c.peekable();
    let mut result = Vec::new();

    loop {
        let next = c.peek();
        match next {
            None => break,
            Some('0'..='9') => result.push(Move::Walk(parse_step(&mut c))),
            Some('R') => {
                c.next();
                result.push(Move::Turn(TurnDirection::Clockwise));},
            Some('L') => {
                c.next();
                result.push(Move::Turn(TurnDirection::Counter));},
            _ => return Err(String::from("Syntax error: unexpected token '{next}'")),
        }
    }

    Ok(result)
}

fn parse_step(c: &mut Peekable<std::str::Chars>) -> usize {
    let mut integer = String::from("");

    loop {
        let next = c.peek();

        match next {
            Some(digit @ '0'..='9') => integer.push(*digit),
            _ => break,
        }

        c.next();
    }

    if integer.len() == 0 {
        panic!{"Internal error: used parse_step and the current value is not a digit."};
    }

    integer.parse::<usize>().unwrap()
}

pub mod utils {
    use super::{Config, Cell, TurnDirection};


    pub fn find_starting_position(map: &Vec<Vec<Cell>>) -> Result<usize, String> {
        let first_line = map.get(0).ok_or(String::from("Map is empty"))?;

        for (index, value) in first_line.iter().enumerate() {
            if *value == Cell::Free {
                return Ok(index);
            }
        }

        Err(String::from("Could not find a free cell on top line"))
    }

    pub fn turn(direction: (isize, isize), turn_direction: TurnDirection) -> (isize, isize) {
        match (turn_direction, direction) {
            (TurnDirection::Clockwise, (y, x)) => (x, y * -1),
            (TurnDirection::Counter, (y, x)) => (x * -1, y),
        }
    }

    pub fn wrap(map: &Vec<Vec<Cell>>, position: (usize, usize), direction: (isize, isize)) -> (usize, usize) {
        let direction = (direction.0 * -1, direction.1 * -1);
        let mut current = position;
        loop {
            let next = ( (current.0 as isize + direction.0), (current.1 as isize + direction.1));

            if next.0 < 0 || next.1 < 0 || next.0 >= map.len() as isize || next.1 >= map[next.0 as usize].len() as isize {
                return current;
            }

            let next = (next.0 as usize, next.1 as usize);

            if map[next.0][next.1] == Cell::Empty {
                return current;
            }

            current = next;
        }
    }
}

pub mod part_one {
    use super::*;

    pub fn solve(config: Config) -> Result<usize, String> {

        // Tuples of (usize, usize) and (isize, isize) represent (y, x) coordinates.

        let mut current = (0, utils::find_starting_position(&config.map)?);
        let mut direction : (isize, isize) = (0, 1);

        for movement in config.path {
            match movement {
                Move::Walk(steps) => for _ in 0..steps {
                    let next = ((current.0 as isize + direction.0), (current.1 as isize + direction.1));

                    if next.0 < 0 || next.1 < 0 || next.0 >= config.map.len() as isize || next.1 >= config.map[next.0 as usize].len() as isize {
                        current = utils::wrap(&config.map, current, direction);
                        continue;
                    }

                    let next = (next.0 as usize, next.1 as usize);
                    match config.map[next.0][next.1] {
                        Cell::Wall => break,
                        Cell::Free => current = next,
                        Cell::Empty=> current = utils::wrap(&config.map, current, direction),
                    }
                },
                Move::Turn(turn_dir) => direction = utils::turn(direction, turn_dir),
            }
        }

        let direction_score = match direction {
            (0, 1) => 0,
            (1, 0) => 1,
            (0,-1) => 2,
            (-1,0) => 3,
            _ => panic!{"Unexpected direction"},
        };

        current = (current.0 + 1, current.1 + 1);
        Ok(current.0 * 1000 + current.1 * 4 + direction_score)
        //Err(String::from(""))
    }

}

#[cfg(test)]
mod test_part_one {
    use super::*;

    #[test]
    fn part_one_sample() -> Result<(), String> {
        let contents = fs::read_to_string("./monkey_map_sample.txt").expect("Could not find file");
        let config = parse_input(contents)?;
        let result = part_one::solve(config)?;
        assert_eq!{result, 6032};
        Ok(())
    }

    #[test]
    fn part_one_input() -> Result<(), String> {
        let contents = fs::read_to_string("./monkey_map.txt").expect("Could not find file");
        let config = parse_input(contents)?;
        let result = part_one::solve(config)?;
        println!{"{result}"};
        Ok(())
    }
}
